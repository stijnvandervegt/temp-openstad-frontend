module.exports = {
  name: "openstad-frontend",
  script: "../app.js",
  time: true,
  node_args: "--max_old_space_size=4096",
  env: {
    NODE_ENV: "development",
    PORT: 8309,
    DEFAULT_HOST: "http://localhost:8309",
    DEFAULT_DB: "openstad",
    SAMPLE_DB: "openstad",
    APP_URL: "http://localhost:8309",
    API: "http://localhost:8311",
    API_LOGOUT_URL: "http://localhost:8311/logout?clientId=918d5290daa23e5b3ee286149c66f1f4",
    IMAGE_API_URL: "https://image-server.staging.openstadsdeel.nl",
    IMAGE_API_ACCESS_TOKEN: "VkajDDVm0KnDaajsjA23AdS",
    SESSION_SECRET: "2dasdasfffh2",
    SITE_API_KEY: "rAjCAuKOIUBilaEluiH7iIIdk3jljsZ5q8RTb0RzGEfrm9VWO3t9284PYMOak8o1v6VUTOJbuQjLSCRwJxRQiTMJa088IllVmz9HyrBAHwfzw1EMrSHI2y63OK0kGNJmPMKS04gVRl3uRCaE20dubcEchF1hTZZonyYqKeraFapib3WchNXfkBGkoZv9sioQEFnB4E6gO0F0CC08c4npdbm97A1gw6Nj2G0nwLvdBTOQLY5D4NMO2uFoF21squRBkOnvbP6HOgDtC3fMqyBtKwuiIkInFN4apnVqmdDtUHvTNbbkrgdOM99Hw12wa5t6oKkpnvf5dD4KvZV4B5KGObbH1aFf6wNjwzKxZfTv5BTLb5xJzAoWdzzxmr9X2AZXYFQ6W5xoJrMuxx3HE3DPAFvK74ePo5BavTgx0RaVpxZNxR2jHkcwPXo7HfeT3b2ySJjMYzC2u8pwelmOynlGuPkKdYvWzhzQhfv4I3DDganCHV4qncccudpd9vVbdS8O",
    GOOGLE_MAPS_API_KEY: "AIzaSyAU1BrnBc0QW9PDai7hpRU2yYpoGXNDnU4"

  },
  env_staging: {
    NODE_ENV: "development",
    PORT: 9519,
    DEFAULT_HOST: "https://frontend.openstad.draad.nu",
    DEFAULT_DB: "openstadstaging",
    SAMPLE_DB: "openstadstaging",
    APP_URL: "https://frontend.openstad.draad.nu",
    API: "https://api.openstad.draad.nu",
    API_LOGOUT_URL: "https://api.openstad.draad.nu/logout?clientId=zZrx9DkFkAa3vwX9BB9r6rrt58G8x1",
    IMAGE_API_URL: "https://image-server.staging.openstadsdeel.nl",
    IMAGE_API_ACCESS_TOKEN: "VkajDDVm0KnDaajsjA23AdS",
    SESSION_SECRET: "qqkpjqEjMY32VQhN35WAcKVZtRdRTM",
    SITE_API_KEY: "rAjCAuKOIUBilaEluiH7iIIdk3jljsZ5q8RTb0RzGEfrm9VWO3t9284PYMOak8o1v6VUTOJbuQjLSCRwJxRQiTMJa088IllVmz9HyrBAHwfzw1EMrSHI2y63OK0kGNJmPMKS04gVRl3uRCaE20dubcEchF1hTZZonyYqKeraFapib3WchNXfkBGkoZv9sioQEFnB4E6gO0F0CC08c4npdbm97A1gw6Nj2G0nwLvdBTOQLY5D4NMO2uFoF21squRBkOnvbP6HOgDtC3fMqyBtKwuiIkInFN4apnVqmdDtUHvTNbbkrgdOM99Hw12wa5t6oKkpnvf5dD4KvZV4B5KGObbH1aFf6wNjwzKxZfTv5BTLb5xJzAoWdzzxmr9X2AZXYFQ6W5xoJrMuxx3HE3DPAFvK74ePo5BavTgx0RaVpxZNxR2jHkcwPXo7HfeT3b2ySJjMYzC2u8pwelmOynlGuPkKdYvWzhzQhfv4I3DDganCHV4qncccudpd9vVbdS8O",
    GOOGLE_MAPS_API_KEY: "AIzaSyAU1BrnBc0QW9PDai7hpRU2yYpoGXNDnU4"

  },
  env_production: {
    NODE_ENV: "production",
  }
};
